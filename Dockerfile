FROM open-liberty:microProfile2
MAINTAINER IBM Java engineering at IBM Cloud
USER root
# The name of the server package file produced by the build
ARG PACKAGE_FILE

# Install unzip and curl
RUN apt-get update \
    && apt-get install -y --no-install-recommends unzip curl\
    && rm -rf /var/lib/apt/lists/* 

COPY target/12factor-app-b-1.0-SNAPSHOT.war /config/
COPY target/12factor-app-b-1.0-SNAPSHOT.zip /config/

# Extract the server package and move files to the correct locations
RUN unzip /config/12factor-app-b-1.0-SNAPSHOT.war \
    && unzip /config/12factor-app-b-1.0-SNAPSHOT.zip \
    && cp -r /wlp/usr/servers/defaultServer/* /config/ \
    && rm -rf /config/wlp \
    && rm -rf /config/12factor-app-b-1.0-SNAPSHOT

EXPOSE 9081 9443
